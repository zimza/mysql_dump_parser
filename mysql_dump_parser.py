#!/usr/local/bin/python3
# coding=utf-8
import os
import argparse
from datetime import datetime
import mmap


def _args_parse():
    parser = argparse.ArgumentParser(description="""MySQL Dump Parser""")

    parser.add_argument("-dump_file", "-d", type=str, help="Path to dump file", required=True)
    parser.add_argument("-str_to_find", "-s", type=str, help="String to find", required=True)
    parser.add_argument("-encoding", "-e", type=str, help="Encoding. Default: UTF-8", default="UTF-8")
    parser.add_argument('-debug', action='store_true', default=False, help="Print results - No results file")

    return parser.parse_args()


def find_all_occurences_in_mmap():
    mmap_length = dump_mm.size()
    found_list = []
    found = -1
    string_to_find = bytes(_str_to_find, _encoding)

    while found < mmap_length:
        found = dump_mm.find(string_to_find, found + 1)
        if found != -1:
            found_list.append(found)
        else:
            return found_list


def is_strictly_delimited_string():
    dump_mm.seek(found_str_start - 1)
    value_prefix = dump_mm.read(1).decode("utf-8")

    if value_prefix in value_delimiters:
        dump_mm.seek(found_str_start + len(_str_to_find))
        value_sufix = dump_mm.read(1).decode("utf-8")

        if value_sufix == value_prefix:
            return 1
        else:
            return 0
    else:
        return 0


def extract_values():
    extracted_values = None

    opening_values_parenthesis = dump_mm.rfind(b'),(', 0, found_str_start) + 2
    opening_values_statement = dump_mm.rfind(b'VALUES (', 0, found_str_start) + 7

    opening_values_delimiter = opening_values_parenthesis if opening_values_parenthesis > opening_values_statement else opening_values_statement

    if opening_values_delimiter != -1:
        closing_values_parenthesis = dump_mm.find(b'),(', found_str_start)
        closing_values_statement = dump_mm.find(b');', found_str_start)

        closing_values_delimiter = closing_values_parenthesis if closing_values_parenthesis < closing_values_statement else closing_values_statement

    if closing_values_delimiter != -1:
        values_length = closing_values_delimiter - opening_values_delimiter
        dump_mm.seek(opening_values_delimiter)
        extracted_values = dump_mm.read(values_length + 1).decode("utf-8")

    return extracted_values, opening_values_delimiter


def extract_insert():
    extracted_insert = None

    insert_start = dump_mm.rfind(insert_string, 0, opening_values_delimiter)

    if insert_start != -1:
        insert_end = dump_mm.find(values_string, insert_start) + 6

        if insert_end:
            insert_length = insert_end - insert_start
            dump_mm.seek(insert_start)
            extracted_insert = dump_mm.read(insert_length).decode("utf-8")

    if extracted_insert not in inserts_list.keys():
        inserts_list[extracted_insert] = []

    return extracted_insert


if __name__ == '__main__':
    _args = _args_parse()
    _dump_file = _args.dump_file
    _str_to_find = _args.str_to_find
    _encoding = _args.encoding
    _debug = _args.debug

    insert_string = bytes('INSERT INTO', _encoding)
    values_string = bytes('VALUES', _encoding)
    values_delimiter_between = bytes('),(', _encoding)
    values_delimiter_start = bytes('VALUES (', _encoding)
    values_delimiter_end = bytes(');', _encoding)

    results_f = None
    inserts_list = {}
    value_delimiters = ['"', "'"]

    # Open dump file and mmap it
    with open(_dump_file, 'r', encoding=_encoding) as dump_f:
        dump_mm = mmap.mmap(dump_f.fileno(), 0, prot=mmap.PROT_READ)

    found_list = find_all_occurences_in_mmap()

    for found_str_start in found_list:
        if is_strictly_delimited_string():
            extracted_values, opening_values_delimiter = extract_values()

            if extracted_values:
                extracted_insert = extract_insert()

                if extracted_insert:
                    inserts_list[extracted_insert].append(extracted_values)

    # Create result file
    if not _debug:
        results_filename = "results_{}_{}.sql".format(os.path.splitext(_dump_file)[0], datetime.now().strftime("%Y%m%d%H%M%S"))
        results_f = open(results_filename, "a+")

    for insert in inserts_list.keys():
        insert_str = "{_insert} {_values};".format(_insert=insert,
                                                   _values=",".join(inserts_list[insert]))

        if _debug:
            print(insert_str)
        else:
            if not results_f and not _debug:
                results_filename = "results_{}_{}.sql".format(os.path.splitext(_dump_file)[0], datetime.now().strftime("%Y%m%d%H%M%S"))
                results_f = open(results_filename, "a+")
            results_f.write(f"{insert_str}\n")

    if results_f and not _debug:
        results_f.close()
